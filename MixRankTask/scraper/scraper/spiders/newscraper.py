# -*- coding: utf-8 -*-
import base64
import os
import re
import scrapy


class NewscraperSpider(scrapy.Spider):
    name = "newscraper"
    with open("websites.csv", "r") as file:
        search_url = ["https://" + line.strip() for line in file]
        domain = re.findall("https:\/\/(.+?)'", str(search_url))
        allowed_domains = domain


    def start_requests(self):
        headers = {
            "User-Agent":
                "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko)"
                " Chrome/44.0.2403.157 Safari/537.36"
        }
        for rec in range(len(self.search_url)):
            url = self.search_url[rec]
            domain_value = self.allowed_domains[rec]
            yield scrapy.Request(url, meta={"domain_value": domain_value}, headers=headers, dont_filter=True,
                                 callback=self.parse_init)



    def parse_init(self, response):
        domain_value = response.meta["domain_value"]
        # Create the directory reports to store the logo image
        path = 'reports'

        try:
            os.mkdir(path)
        except OSError as error:
            print(error)

        items = {}
        name_image = str(response.url).replace('https://', '').replace('www', '')
        name_image = name_image[1:4]

        if 'uk2.net' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Uk2 Logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'cio.com' in response.url:
            # Slice the name and store the image in format svg
            svg_name = name_image
            logo_url_data = response.xpath('//a[@aria-label="Home page"]//svg').extract_first()
            with open('reports/' + str(svg_name) + '.png', "w") as data:
                data.write(logo_url_data)


        # In this example I have used base64 to decode the image
        if 'bounteous.com' in response.url:
            # Slice the name and store the image in format svg
            svg_name = name_image
            logo_url_data = response.xpath('//div[@class="Navbar-module__logo--Jg-ze"]//a//img//@src').extract_first()
            image_data = re.findall('base64,([\s\S]+)', str(logo_url_data))[0]
            decodeddata = base64.b64decode(str(image_data))
            imgFile = open('reports/' + str(svg_name) + '.png', 'wb')
            imgFile.write(decodeddata)
            imgFile.close()

        if 'tibco.com' in response.url:
            # Slice the name and store the image in format svg
            logo_url_data = response.xpath('//span[@class="logo-holder"]//svg').extract_first()
            svg_name = name_image
            with open('reports/' + str(svg_name) + '.png', "w") as data:
                data.write(logo_url_data)


        if 'sitepoint.com' in response.url:

            # Slice the name and store the image in format svg
            logo_url_data = response.xpath('//a[@aria-label="SitePoint"]//svg').extract_first()
            svg_name = name_image
            with open('reports/' + str(svg_name) + '.png', "w") as data:
                data.write(str(logo_url_data))


            # This webiste throws an error 403 so used rotating proxies to resolve the issues
        if 'bandwidth.com' in response.url:
            # Slice the name and store the image in format svg

            logo_url_data = response.xpath('//a[@class="header-logo-link"]//svg').extract_first()
            svg_name = name_image
            with open('reports/' + str(svg_name) + '.png', "w") as data:
                data.write(logo_url_data)


        if 'whmcs' in response.url:
            logo_url = response.urljoin(response.xpath('//link[@rel="apple-touch-icon"]//@href').extract_first())
            items['logo_url'] = logo_url



        if 'blurbpoint' in response.url:
            logo_url = response.xpath('//a[@class="white-clr"]//img[@alt="Blurbpoint"]'
                                      '//@data-lazy-src').extract_first()
            items['logo_url'] = logo_url


        if 'www.sogolytics.com' in response.url:
            logo_url = response.xpath('//img[@class="logo_retina_transparent"]//@src').extract_first()
            items['logo_url'] = logo_url

        if 'weaknees.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="WeaKnees TiVo Upgrade Kits"]'
                                                       '//@src').extract_first())
            items['logo_url'] = logo_url


        if 'thebigboss.org' in response.url:
            logo_url = response.urljoin(response.xpath('//div[@id="header-info"]//img[1]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'www.portent.com' in response.url:
            logo_url = response.xpath('//img[@alt="Portent"]//@src').extract_first()
            items['logo_url'] = logo_url


        if 'pcb.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="logo-img"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'www.bravenet.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Bravenet"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'sudokuprofessor.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="SPLogo - Recolor - Tiny 75"]'
                                                       '//@src').extract_first())
            items['logo_url'] = logo_url


        if 'activecampaign.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="ActiveCampaign Logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'letsrev.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="rev logo"]//@data-src').extract_first())
            items['logo_url'] = logo_url


        if 'voipsupply.com' in response.url:
            logo_url = response.urljoin(response.xpath('//a[@class="logo"]//img//@src').extract_first())
            items['logo_url'] = logo_url

        if 'matomo.org' in response.url:
            logo_url = response.xpath('//img[@class="mega-menu-logo"][1]//@src').extract()[1]
            items['logo_url'] = logo_url


        if '13tv.co.il' in response.url:
            logo_url = response.xpath('//a[contains(@class, "Headerstyles__LogoLink-sc-")]//img//@src').extract()[1]
            items['logo_url'] = logo_url


        if '4team.biz' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@itemprop="logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'sphero.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="header__logo-image"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'iana.org' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="header__logo-image"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'listrak.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="logo-icon"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'wirelessemporium.com' in response.url:
            logo_url = response.urljoin(response.xpath('//a[@class="desktop-logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'wordstream.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="WordStream"][1]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'smtp.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="SMTP email delivery"][1]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'aptum.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Aptum Logo"][1]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'virginmedia.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Virgin Media"][1]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'airship.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Airship"][1]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'eztexting.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="SMS Marketing by EZ Texting"][1]'
                                                       '//@src').extract_first())
            items['logo_url'] = logo_url


        if 'cloudera.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Cloudera"][1]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'homesandland' in response.url:
            logo_url = response.urljoin(response.xpath('//meta[@property="og:image"]//@content').extract_first())
            items['logo_url'] = logo_url

        if 'broadvoice.com' in response.url:
            logo_url = response.xpath('//img[@alt="Broadvoice"]//@src').extract()[1]
            items['logo_url'] = logo_url


        if 'citymax.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="citymaxlogo"][1]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'geotrust.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="GeoTrust Powered By DigiCert Logo"]'
                                                       '[1]//@src').extract_first())

        if 'godatafeed.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="nav04_image"][1]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'gogoair.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Gogo"][1]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'currently.att.yahoo.com' in response.url:
            logo_url = response.urljoin(response.xpath('//a[@id="ybar-logo"][1]//img[1]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'ntt.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="docomo business | NTT Communications"]'
                                                       '//@src').extract_first())
            items['logo_url'] = logo_url


        if 'equinix.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt=" title logo image"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'speakeasy.net' in response.url:
            logo_url = response.urljoin(response.xpath('//div[@class="logo-contain"]//img[@alt="Fusion Connect"][1]'
                                                       '//@src').extract_first())
            items['logo_url'] = logo_url


        if 'so-net.ne.jp' in response.url:
            logo_url = response.urljoin(response.xpath('//h1[@class="media logo"][1]//img[@class="logo-sonet"][1]'
                                                       '//@src').extract_first())
            items['logo_url'] = logo_url

        if 'rackspace.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="nav__logo-img"][1]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'inap.com' in response.url:
            logo_url = response.urljoin(response.xpath('//a[@class="navbar-brand"][1]//img[@alt="INAP"][1]'
                                                       '//@src').extract_first())
            items['logo_url'] = logo_url


        if 'inap.com' in response.url:
            logo_url = response.urljoin(
                response.xpath('//a[@class="navbar-brand"][1]//img[@alt="INAP"][1]//@src').extract_first())
            items['logo_url'] = logo_url


        if 't-online.de' in response.url:
            logo_url = response.urljoin(
                response.xpath('//img[@class="css-4zleql"][1]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'leaseweb.com' in response.url:
            logo_url = response.urljoin(
                response.xpath('//a[@class="logo-link"][1]//img//@src').extract_first())
            items['logo_url'] = logo_url

        if 'moonfruit.com' in response.url:
            logo_url = response.urljoin(
                response.xpath('//img[@alt="MF logo_edited.png"][1]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'sangoma.com' in response.url:
            logo_url = response.urljoin(
                response.xpath('//div[@class="elementor-image"]//img[contains(@class, "attachment-large size")]'
                               '[1]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'discounttwo-wayradio.com' in response.url:
            logo_url = response.urljoin(
                response.xpath('//img[contains(@class, "cpw-logo")][1]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'mitel.com' in response.url:
            logo_url = response.urljoin(
                response.xpath('//img[contains(@class, "full-logo")][1]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'imation.com' in response.url:
            logo_url = response.urljoin(
                response.xpath('//a[@class="logo"]//img//@src').extract_first())
            items['logo_url'] = logo_url

        if 'nobelcom.com' in response.url:
            logo_url = response.urljoin(
                response.xpath('//img[@itemprop="logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'nobelcom.com' in response.url:
            logo_url = response.urljoin(
                response.xpath('//img[@itemprop="logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'new.talktalk.co.uk' in response.url:
            logo_url = response.urljoin(
                response.xpath('//img[@itemprop="logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'alexa.com' in response.url:
            logo_url = response.urljoin(
                response.xpath('//img[@alt="alexa_logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'yahoo.com' in response.url:
            logo_url = response.urljoin(
                response.xpath('//a[@id="ybar-logo"]//img[1]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'conductor.com' in response.url:
            logo_url = response.urljoin(
                response.xpath('//img[@alt="Conductor"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'ampedwireless.com' in response.url:
            logo_url = response.urljoin(
                response.xpath('//img[@alt="Amped Wireless"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'netfirms.com' in response.url:
            logo_url = response.urljoin(
                response.xpath('//img[@alt="Netfirms Logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'unbounce.com' in response.url:
            logo_url = (response.xpath('//img[@alt="unbounce"]//@src').extract()[0])
            items['logo_url'] = logo_url


        if 'netvibes.com' in response.url:
            logo_url = response.xpath('//img[@alt="NETVIBES logo"]//@src').extract_first()
            items['logo_url'] = logo_url


        if 'photobiz.com' in response.url:
            logo_url = response.xpath('//picture[@class="logo__picture"][1]//img[@alt="logo"]//@src').extract_first()
            items['logo_url'] = logo_url


        if 'enterprise.efax.co' in response.url:
            logo_url = response.xpath('//img[@alt="logo"]//@src').extract_first()
            items['logo_url'] = logo_url

        if 'arubanetworks.com' in response.url:
            logo_url = response.xpath('//img[@alt="Aruba"]//@src').extract_first()
            items['logo_url'] = logo_url


        if 'bodyguardz.com' in response.url:
            logo_url = response.urljoin(response.xpath('//a[contains(@class, "brand-logo")][1]//img'
                                                       '//@src').extract_first())
            items['logo_url'] = logo_url

        if 'pinnaclecart.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="PinnacleCart Logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'echonewyork.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Echo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'signupgenius.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="SignUpGenius"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'tripod.lycos.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Tripod.com logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'inmobi.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="InMobi"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'inmobi.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="InMobi"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'artistwebsites.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Artist Websites - Websites for Artists"]'
                                                       '//@src').extract_first())
            items['logo_url'] = logo_url


        if 'inmobi.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="InMobi"]//@src').extract_first())
            items['logo_url'] = logo_url


        if '5gstore.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="5Gstore Logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'netsuite.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'cafe24.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="cafe24"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'tradeindia.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'flippingbook.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="FlippingBook logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'latimes.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="PageLogo-image"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'yahoo.com' in response.url:
            logo_url = response.urljoin(response.xpath('//a[@id="ybar-logo"][1]//img[1]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'theologyofwork.org' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Theology of Work: The High Calling"]'
                                                       '//@src').extract_first())
            items['logo_url'] = logo_url


        if 'facebook.com' in response.url:
            logo_url = response.xpath('//img[@alt="facebook"]//@src').extract_first()
            items['logo_url'] = logo_url

        if 'trendhunter.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Trend Hunter Logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'vidyo.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Vidyo Logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'intermedia.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="intermedia logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'intermedia.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="intermedia logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'lg.com' in response.url:
            logo_url = response.urljoin(response.xpath('//a[@data-link-area="gnb_brand_identity"]//img'
                                                       '//@src').extract_first())
            items['logo_url'] = logo_url

        if 'yesware.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="logo-brand"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'hostdime.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="logo_full"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'rakuten.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Rakuten Logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'relianceglobalcall.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Prepaid international calling cards"]'
                                                       '//@src').extract_first())
            items['logo_url'] = logo_url

        if 'clickmeter.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@id="logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'ignitionone.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@id="logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'kvh.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="KVH Industries"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'local03.com' in response.url:
            logo_url = response.urljoin(response.xpath('//li[@class="name"]//a//img//@src').extract_first())
            items['logo_url'] = logo_url


        if 'tributes.com' in response.url:
            logo_url = response.urljoin(response.xpath('//div[@name="logo"]//img//@src').extract_first())
            items['logo_url'] = logo_url


        if 'slickdeals.net' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Slickdeals Logotype"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'tollfreeforwarding.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="img-fluid"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'smaato.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Smaato"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'vps.net' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="VPS.net Logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'sewelldirect.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="site-logo-image"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'hostbaby.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Bandzoogle logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'hostbaby.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Bandzoogle logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'swisscom.ch' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Bandzoogle logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'sinovision.net' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'line2.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Line2 logo"]//@data-src').extract_first())
            items['logo_url'] = logo_url


        if 'go.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Disney"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'godaddy.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Disney"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'directv.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="DIRECTV home page"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'whitepages.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="DIRECTV home page"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'surveymonkey.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="SurveyMonkey Logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 't-mobile.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="T-Mobile"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'shopify.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="SurveyMonkey Logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'spokeo.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="SurveyMonkey Logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'constantcontact.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="ctct-logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'zoho.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="ctct-logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'wix.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="ctct-logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'goto.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="ctct-logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'dish.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="DISH company logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'epsilon.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="eps-logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'webs.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="ctct-logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'salesforce.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Salesforce Home"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'centurylink.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="CenturyLink Logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'motorola' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Motorola logo"]//@data-src').extract_first())
            items['logo_url'] = logo_url

        if 'localiq.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="LocaliQ Logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'rackspace.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="nav__logo-img"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'mobile.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="metro by T-Mobile"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'verticalresponse.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Home"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'webex.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Webex"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'webex.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Webex"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'sulekha.com' in response.url:
            logo_url = response.urljoin(response.xpath('//link[@rel="shortcut icon"]//@href').extract_first())
            items['logo_url'] = logo_url

        if 'formstack.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="nav-logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'ning.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@itemprop="logo"]//@src').extract_first())
            items['logo_url'] = logo_url


        if 'avaya.com' in response.url:
            logo_url_1 = response.xpath('//div[@class="av-logo"]//a//svg//use').extract_first()
            logo_url = re.findall('xlink:href="(.+?)"',str(logo_url_1))[0]
            items['logo_url'] = logo_url

        if 'bigcommerce.com' in response.url:
            logo_url = response.xpath('//img[@title="Bc logo dark"]//@url').extract_first()
            items['logo_url'] = logo_url

        if 'nokia.com' in response.url:
            logo_url = response.xpath('//a[@class="site-logo nokia-logo"]//img//@src').extract_first()
            items['logo_url'] = logo_url

        if 'univision.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="sc-1fz50yy-5 bmvnSk"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'jawbone.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@data-load="false"]//@data-src').extract_first())
            items['logo_url'] = logo_url

        if 'univision.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="sc-1fz50yy-5 bmvnSk"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'networksolutions.com' in response.url:
            logo_url = response.urljoin(response.xpath('//a[@class="cmp-image__link"]//img[@class="cmp-image__image"]'
                                                       '//@src').extract_first())
            items['logo_url'] = logo_url

        if 'sony.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Electronics Sony Home"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'sharefile.com' in response.url:
            logo_url = response.urljoin(response.xpath('//a[@class="sharefile-logo"]//img//@src').extract_first())
            items['logo_url'] = logo_url

        if 'marinsoftware.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@class="marin-logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'mail.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="mail.com logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'hughesnet.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Home"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'neat.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Neat"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'peoplesmart.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="PeopleSmart logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'fusionconnect.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Fusion Connect"]//@src').extract_first())
            items['logo_url'] = logo_url

        if 'wolterskluwer.com' in response.url:
            logo_url = response.urljoin(response.xpath('//img[@alt="Wolters Kluwer Logo"]//@src').extract_first())
            items['logo_url'] = logo_url

        else:
            try:
                items['logo_url']
            except:
                logo_url = response.urljoin(response.xpath('//link[contains(@rel, "icon")]//@href').extract_first())
                if logo_url == '':
                    logo_url = response.urljoin(response.xpath('//meta[@property="og:image"]'
                                                               '//@content').extract_first())
                items['logo_url'] = logo_url
        items['domain_value'] = domain_value
        items['source_url'] = response.url

        yield scrapy.Request(items['logo_url'], meta={"items": items, "name_image": name_image, },  dont_filter=True,
                             callback=self.parse_search)

    def parse_search(self, response):
        name_image = response.meta["name_image"]
        items = response.meta["items"]
        yield items
        data = response.body
        # # This file would store the data of the image file
        f = open('reports/' + str(name_image) + '.png', 'wb')
        # Storing the image data inside the data variable to the file
        f.write(data)
        f.close()


















